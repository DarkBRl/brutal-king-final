﻿# The script of the game goes in this file.

# Declare characters used by this game. The color argument colorizes the
# name of the character.
define R = Character("Rangi", who_color="#00FA9A")
image Room = "background_teste.jpg"
image Rangi = "Rangi.png"
image Rangi_H = "Rangi_2.jpg"
# The game starts here.

label start:
    play music "../audio/Sadness.mp3"
    show Room #
    # These display lines of dialogue.
    
    show Rangi #mostra a imagem.

    R "You've created a new Ren'Py game."
    hide Rangi #esconde a imagem.
    
    show Rangi_H

    R "Once you add a story, pictures, and music, you can release it to the world!"
    stop music
    show Rangi
    
    hide Rangi
    hide Rangi_H
    
    show Room
    "Narrador TESTE"
    
    show Rangi at right # escolhe o local da tela que o personagem vai aparecer Direita
    show Rangi_H at left # Esquerda
    "EI"
    show Rangi at center # Centro
    "TESTE"

    # This ends the game.

    return
